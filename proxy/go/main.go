package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "HELLO FROM GO IN NGINX TO DOCKER TO %s", r.RemoteAddr)
	})
	http.ListenAndServe(":80", nil)
}
